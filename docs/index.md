![logo do projeto](assets/logo.png){ width="200" .center }
# Notas Musicais

## Como usar?

Você pode chamar as escalas via linha de comando. Por exemplo:
```bash
poetry run escalas
```
Retornando as notas e os respectivos graus da escala:

```bash
┏━━━┳━━━━┳━━━━━┳━━━━┳━━━┳━━━━┳━━━━━┓
┃ I ┃ II ┃ III ┃ IV ┃ V ┃ VI ┃ VII ┃
┡━━━╇━━━━╇━━━━━╇━━━━╇━━━╇━━━━╇━━━━━┩
│ C │ D  │ E   │ F  │ G │ A  │ B   │
└───┴────┴─────┴────┴───┴────┴─────┘

```

### Alterações na escala

O primeiro parâmetro do CLI é a nota tônica da escala que se deseja exibir. Desta forma, você pode alterar a escala retornada. Por exemplo, para exibir a escala de Fá sustenido, use `F#`:

```bash
poetry run escalas F#
```

Resultando em:
```bash
┏━━━━┳━━━━┳━━━━━┳━━━━┳━━━━┳━━━━┳━━━━━┓
┃ I  ┃ II ┃ III ┃ IV ┃ V  ┃ VI ┃ VII ┃
┡━━━━╇━━━━╇━━━━━╇━━━━╇━━━━╇━━━━╇━━━━━┩
│ F# │ G# │ A#  │ B  │ C# │ D# │ F   │
└────┴────┴─────┴────┴────┴────┴─────┘
```

### Alterações na tonalidade da escala

O segundo parâmetro do CLI é usado para alterar a tonalidade da escala. Por exemplo, a escala de `D#` maior:

```bash
poetry run escalas D# maior
```

Resultando em:
```bash
┏━━━━┳━━━━┳━━━━━┳━━━━┳━━━━┳━━━━┳━━━━━┓
┃ I  ┃ II ┃ III ┃ IV ┃ V  ┃ VI ┃ VII ┃
┡━━━━╇━━━━╇━━━━━╇━━━━╇━━━━╇━━━━╇━━━━━┩
│ D# │ F  │ G   │ G# │ A# │ C  │ D   │
└────┴────┴─────┴────┴────┴────┴─────┘
```

### Mais informaçãoes sobre o CLI

Para mais opções da linha de comando, você pode usar a flag `--help`

```bash
poetry run escalas --help
                                                                                
 Usage: escalas [OPTIONS] [TONICA] [TONALIDADE]                                 
                                                                                
╭─ Arguments ──────────────────────────────────────────────────────────────────╮
│   tonica          [TONICA]      Tônica da escala [default: c]                │
│   tonalidade      [TONALIDADE]  Tonalidade da escala [default: maior]        │
╰──────────────────────────────────────────────────────────────────────────────╯
```
